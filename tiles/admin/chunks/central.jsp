<!-- start: Content -->
<div id="content" class="span11">

  <ul class="breadcrumb">
    <li>
      <i class="icon-home"></i>
      <a href="${baseURL}/admin/">Home</a>
      <i class="icon-angle-right"></i>
    </li>
    <li><a href="#">Dashboard</a></li>
  </ul>

  <div class="row-fluid sortable ui-sortable">
    <div class="box span11">
      <div class="box-header">
        <h2><i class="halflings-icon align-justify"></i><span class="break"></span>User ads</h2>
        <div class="box-icon">
          <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
          <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
          <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
        </div>
      </div>
      <div class="box-content">
        <div id="alert"></div>
        <table id="adv_ads" class="display cell-border compact" cellpadding="0" data-page-length="25" width="100%">
          <thead>
          <tr>
            <th>User</th>
            <th>Ad Id</th>
            <th>Ad title</th>
            <th>Ad description</th>
            <th>Ad url</th>
            <th>Keywords</th>
            <th>Total price</th>
            <th>Payment status</th>
            <th>Showed on site</th>
          </tr>
          </thead>
          <tfoot>
          <tr>
            <th>User</th>
            <th>Ad Id</th>
            <th>Ad title</th>
            <th>Ad description</th>
            <th>Ad url</th>
            <th>Keywords</th>
            <th>Total</th>
            <th>Payment status</th>
            <th>Showed on site</th>
          </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>