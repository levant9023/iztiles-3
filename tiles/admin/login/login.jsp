<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/js/bootstrap.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/css/bootstrap.min.css"/>
  <link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/login.css" />
  <link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/bootstrap-social.css" />
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css"/>
  <title>iZSearch Login</title>
</head>
<body>
<div class="header">
  <a class="logo" href="${baseURL}/admin/"><img src="${baseURL}/resources/img/logo.svg" alt="logo"><br>© iZSearch</a>
  <h1>Login</h1>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-login">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-6">
              <a href="#" class="active" id="login-form-link">Login</a>
            </div>
            <div class="col-xs-6">
              <a href="#" id="register-form-link">Register</a>
            </div>
          </div>
          <hr>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <a style="margin-bottom: 25px" class="btn btn-block btn-social btn-lg btn-facebook"
                 href="${baseURL}/admin/connect/facebook" onclick="_gaq.push(['_trackEvent', 'btn-social', 'click', 'btn-lg']);"><i class="fa fa-facebook"></i>Sign in with Facebook</a>

              <form name="loginForm"  id="login-form" action="javascript://" method="post" role="form" style="display: block;">
                <div class="form-error text-danger message"></div>
                <div class="form-group">
                  <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Email Address" value="">
                  <div class="username-error text-danger message"></div>
                </div>
                <div class="form-group">
                  <input type="password" name="password" tabindex="2" class="form-control" placeholder="Password">
                  <div class="password-error text-danger message"></div>
                </div>
                <div class="form-group text-center">
                  <input type="checkbox" tabindex="3" name="remember" id="remember"> <label for="remember"> Remember Me</label>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                      <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                    </div>
                  </div>
                </div>
                <div class="text-center">
                  <a href="${baseURL}/user/reset" tabindex="5" class="forgot-password">Forgot Password?</a>
                </div>
              </form>
              <%-- action="/user/registration" --%>
              <form name="registerForm" id="register-form" action="javascript://" method="post" role="form" style="display: none;">
                <div class="form-error text-danger message"></div>
                <div class="form-group">
                  <input type="email" name="email" tabindex="1" class="form-control" placeholder="Email Address" value="" />
                  <span class="username-error text-danger message"></span>
                </div>
                <div class="form-group">
                  <input type="password" name="password" tabindex="2" class="form-control" placeholder="Password" />
                  <div class="password-error text-danger message"></div>
                </div>
                <div class="form-group">
                  <input type="password" name="matchPassword" tabindex="2" class="form-control" placeholder="Confirm Password" />
                  <div class="confirm-password-error text-danger message"></div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                      <input type="button" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#login-form-link').click(function (e) {
            $("#login-form").delay(100).fadeIn(100);
            $("#register-form").fadeOut(100);
            $('#register-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });

        $('#register-form-link').click(function (e) {
            $("#register-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $('#login-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
    });
</script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mvc/controls/user/admin_login.js"></script>
</body>
</html>

