<%--
  User: efanchik
  Date: 6/8/15
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>

<c:set var="btn_sh" value="${cookie['btn_sh'].value}" scope="page"/>

<c:choose>
  <c:when test="${requestScope.wpath == '/bugreport.html'}">
    <c:set var="requestURL" value="${baseURL}"/>
  </c:when>
  <c:when test="${requestScope.wpath == '/comments.html'}">
    <c:set var="requestURL" value="${baseURL}"/>
  </c:when>
  <c:otherwise>
    <c:set var="requestURL" value="${baseURL}${requestScope.wpath}" scope="request"/>
  </c:otherwise>
</c:choose>

<c:choose>
  <c:when test="${btn_sh eq 1}">
    <c:set var='visible' value='style="display: block"' />
  </c:when>
  <c:otherwise>
    <c:set var='visible' value='style="display: hidden"' />
  </c:otherwise>
</c:choose>


<script type="text/javascript" async data-main="${baseURL}/resources/js/production/izsearch/twoblock/mvc/top_nav.js" src="${baseURL}/resources/js/production/require.js">
</script>

<div id="header" class="row">
  <nav class="navbar navbar-default no-back" role="navigation">
    <div class="container-fluid">
      <c:set var="logo" value="${iz:basename(requestScope['javax.servlet.forward.request_uri'])}" />
      <div id="logo" class="navbar-header">
        <a href="${baseURL}" class="navbar-brand">
          <img class="logo" src="${baseURL}/resources/img/bugreport_line.png" alt="logo">
        </a>
      </div>

      <c:set var="query" value="" />

      <form id="search-form" class="navbar-form navbar-left" role="search"
            action="${requestURL}" method="GET" target="_self">
        <input id="input-search" type="text" class="form-control" autocomplete="off" name="q"
               value="<c:out value="${query}"/>" autofocus>
        <button id="btn-search" type="submit" class="input-inside" style="margin-top: -35px;">
          <img src="${baseURL}/resources/img/lence_search.png" alt="lence">
        </button>
        <div id="search-subpanel" class="form-group">
          <ul id="autocomplete" class="autocomplete"></ul>
        </div>
      </form>
      <div class="collapse navbar-collapse" id="nvb-01">
        <ul id="search-toolbar" class="nav navbar-nav navbar-right SearchNavbar">
          <li class="first" ${visible}>
            <div id="btg-tols" class="btn-group">

              <!-- Redmine button -->
              <div id="btg-bugfix" class="btn-group">
                <a id="remine-link" href="javascript:" class="btn" data-toggle="search-nav"
                   role="popover" data-history="false" title="" data-original-title="Bug report" aria-describedby="popover599443">
                  <div id="btn-top-bugreport" class="btn-nav-img"></div>
                </a>
                <div class="popover-content hide">
                  <div id="redmine-report">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <span class="simg glyphicon glyphicon-send"></span>
                        <h3 class="bugfix-title" style="padding-top: 4px;">Report bugs</h3>
                      </div>
                      <div class="panel-body">
                        <form id="form-redmine" class="izwidget" role="form">
                          <div class="form-group">
                            <label for="inputUserName" class="control-label">Your name (required)*:</label>
                            <input type="text" class="form-control" id="inputUserName" name="username" value="">
                            <span id="username_message" class="message"></span>
                          </div>
                          <div class="form-group">
                            <label for="inputBugEmail" class="control-label">Your email (required)*:</label>
                            <input type="email" class="form-control" id="inputBugEmail" name="usermail">
                            <span id="usermail_message" class="message"></span>
                          </div>
                          <div class="form-group">
                            <label for="inputBugSubject" class="control-label">Subject (required) *:</label>
                            <input type="text" class="form-control" id="inputBugSubject" name="bugsubject">
                            <span id="bugsubject_message" class="message"></span>
                          </div>
                          <div class="form-group">
                            <label for="inputBugDescr" class="control-label">Bug description (required)*:</label>
                            <textarea class="form-control" rows="2" id="inputBugDescr" name="bugdescr"></textarea>
                            <span id="bugdescr_message" class="message"></span>
                          </div>
                          <div class="form-group">
                            <div class="btn-send">
                              <span id="redmine-result-message" class="message"></span>
                              <button id="btn-redmine" type="button" class="btn btn-primary btn-block">
                                <span class="glyphicon glyphicon-envelope left"></span>Send
                                <span class="glyphicon glyphicon-arrow-right right"></span>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Redmine report button -->

              <div id="redmine-report-link" class="btn-group">
                <a id="redmine-blog" href="bugreport.html?page=1" class="btn btn-default btn-sm" title="Bug report and Discussion Forum">
                  <div id="btn-top-redmine" class="btn-nav-img"></div>
                </a>
              </div>
              </div>
          </li>

          <li class="last">
            <div id="btg-sh" class="btn-group">
              <a href="javascript://" class="btn btn-default btn-sm" title="Toolbar with extended option buttons" data-title="Toolbar with extended option buttons" data-toggle="tool-sh" role="popover">
                <div id="btn-top-tools" class="btn-nav-img"></div>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </div>

<!-- /.container-fluid -->
</nav>
</div>