<%--
  Created by IntelliJ IDEA.
  User: efanchik
  Date: 30.01.15
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz"%>

<div class="breadcrumb">
  <c:forEach var="entry" items="${sessionScope.currentBreadCrumb}">
    <c:choose>
      <c:when test="${entry.currentPage == true}">
        ${entry.label}
      </c:when>
      <c:otherwise>
        <a href="${entry.url}">${entry.label}></a>
      </c:otherwise>
    </c:choose>
  </c:forEach>

</div>