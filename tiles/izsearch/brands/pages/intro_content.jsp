<section class="main">
  <div class="container">
    <div class="row"></div>
    <div id="main-title" class="row d-flex justify-content-center">
      <div class="menu-content pb-60 col-lg-12">
        <div class="title text-center">
          <h3 class="mb-10">iZ Brands - INTERNET MONITORING TOOLS FOR YOUR COMPANY</h3>
        </div>
      </div>
    </div>

    <div class="row">
      <ul class="col-md-6">
        <li>
          <div>
            <img class="img-fluid" src="${baseURL}/resources/img/brands/leader.png" alt="">
          </div>
        </li>
        <li>
          <div>
            <h3>Company Leaders</h3>
            <p>
              You have a company or small business and want to monitor in real time what people are saying on-line about your products and brands?<br>
              Understand how the community feels about the changes you made in the last release and see the effectiveness of you current activities.<br>
              Identify sudden changes in the discussion volume to protect your company image. </p>
          </div>
        </li>
      </ul>
      <ul class="col-md-6">
        <li>
          <div>
            <img class="img-fluid" src="${baseURL}/resources/img/brands/marketing.png" alt="">
          </div>
        </li>
        <li>
          <div>
            <h3>Marketing and Sales</h3>
            <p>
              You are the Marketing, Sales, PR or Product Manager and want to get feedback from your users?<br>
              Analyze buzz quality and quantity to gain new insights about your customers and stay up to date with users' opinion changes.<br>
              Detect sales opportunities and improve your product or brand according to what users want.
            </p>
          </div>
        </li>
      </ul>
    </div>


    <div class="row">
      <ul class="col-md-6">
        <li>
          <div>
            <img class="img-fluid" src="${baseURL}/resources/img/brands/competitor.png" alt="">
          </div>
        </li>
        <li>
          <div>
            <h3>Competition Analysis</h3>
            <p>
              Want to compare with your competitors? Track your competition- see what they publish, where they go (places, events, conferences, etc.), what and where they advertise.
              See comparative statistical analysis of how your competitors performed over the same metrics (period of time, users age
              groups, gender, geo-locations, etc.). <br>
              Spot your weaknesses (before your competitors do) and react in real-time
            </p>
          </div>
        </li>
      </ul>
      <ul class="col-md-6">
        <li>
          <div>
            <img class="img-fluid" src="${baseURL}/resources/img/brands/scientist.png" alt="">
          </div>
        </li>
        <li>
          <div>
            <h3>Scientists and Students</h3>
            <p>
              You are a student or scientist and want to do statistical analysis of "drug usage", "poor quality", "bad packaging", "side effects" and other specific details about
              your
              drugs on the market.<br>
              Generate detailed reports (PDF, Excel, Word) with a lot of statistics and infografics over various metrics, such as time, geo location, age groups, gender and other.
            </p>
          </div>
        </li>
      </ul>
    </div>


    <div class="row">
      <ul class="col-md-6">
        <li>
          <div>
            <img class="img-fluid" src="${baseURL}/resources/img/brands/schools.jpg" alt="">
          </div>
        </li>
        <li>
          <div>
            <h3>Universities, Schools and Organisations</h3>
            <p>
              You represent a University/Institution, School or Public Organization and want to know what your (and other) students say about Your University? What are the problems
              and challenges your students are worried about: classrooms, anxiety, violence?
              What are your teachers concerned about? Identify problematic areas and react quickly before they get worse.
            </p>
          </div>
        </li>
      </ul>
      <ul class="col-md-6">
        <li>
          <div>
            <img class="img-fluid" src="${baseURL}/resources/img/brands/celebs.jpg" alt="">
          </div>
        </li>
        <li>
          <div>
            <h3>Celebrities, Politicians and Public Figures</h3>
            <p>
              You are a public person, celebrity, politician or a famous blogger! You are working hard to promote your name in public media and want to monitor the buzz about your name!
              You want to have a political, promotional or marketing campaign! <br> Follow-up on positive comments from your name ambassadors, or respond to a dissatisfied citizens
              before the story gets ahead of you.
              Track your opponents, see fake and real news about you and your competition.
            </p>
          </div>
        </li>
      </ul>
    </div>


    <div class="row">
      <ul class="col-md-6">
        <li>
          <div>
            <img class="img-fluid" src="${baseURL}/resources/img/brands/investor.png" alt="">
          </div>
        </li>
        <li>
      <div>
        <h3>Investors and Traders</h3>
        <p>
          You are an investor and want to invest smartly? <br>Monitor your favorite stocks or companies and see changes (positive or negative) in the publics' opinions about your
          company's products and brands in real time. React quickly before these changes hit the market. </br>
          Are you trading Euro-Dollar pair, want to get all the ECB, FRS, EUR, USD data systematized in one place together with all the analytics and be a step ahead of the
          market/FOREX events?
        </p>
      </div>
        </li>
      </ul>
      <ul class="col-md-6">
        <li>
          <div>
            <img class="img-fluid" src="${baseURL}/resources/img/brands/trading.png" alt="">
          </div>
        </li>
        <li>
          <div>
            <h3>Trading Companies</h3>
            <p>
              You are a trading company and want to provide more analytics tools for stocks and markets on the Web Monitoring, Live News, Social Media insights,
              Sentiment analysis, Statistical tools and Web Infographics?
            </p>
          </div>
        </li>
      </ul>
    </div>

  </div>
</section>


<section class="about" id="about">
  <div class="container text-center">

    <div class="row stats-row">
      <div class="stats-col text-center col-md-3 col-6">
        <div class="circle">
          <span class="stats-no" data-toggle="counter-up">7,500</span> Companies
        </div>
      </div>

      <div class="stats-col text-center col-md-3 col-6">
        <div class="circle">
          <span class="stats-no" data-toggle="counter-up">39,600</span> Brands and Products
        </div>
      </div>

      <div class="stats-col text-center col-md-3 col-6">
        <div class="circle">
          <span class="stats-no" data-toggle="counter-up">30,000,000</span> Resources processed daily
        </div>
      </div>

      <div class="stats-col text-center col-md-3 col-6">
        <div class="circle">
          <span class="stats-no" data-toggle="counter-up">5,000,000,000</span> Daily mentions
        </div>
      </div>
    </div>
  </div>
</section>


<section id="owl1" class="owl">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-sm-12 text-center">
        <div class="owl-carousel">

          <div><img src="${baseURL}/resources/img/brands/owl/underarmour.png"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/wholefoodsmarket.png"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/adidas.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/hilton_icon_image_cms.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/ucsd.jpg"></div>

          <div><img src="${baseURL}/resources/img/brands/owl/johnson_s.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/l_oreal.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/nike.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/starbucks2.png"></div>

          <div><img src="${baseURL}/resources/img/brands/owl/redbulllogo.png"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/marriott_icon_image_cms.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/rolex.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/mercedes1_logo_cms.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/playstation22.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/walmart_logo_cms.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/lenovo2.png"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/chanel3.png"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/bayer_logo_cms.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/targetlogo.png"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/qualcomm_logo_cms.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/pampers.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/lego.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/kfc_logo_cms.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/daew_8.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/emirates2.png"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/nescafe3.png"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/20th_century_fox_logo.jpg"></div>
          <div><img src="${baseURL}/resources/img/brands/owl/pantene2.png"></div>

        </div>
      </div>
    </div>
  </div>
</section>
<!-- Parallax -->

<div id="paralax-block" class="block bg-primary block-pd-lg block-bg-overlay text-center" data-bg-img="${baseURL}/resources/img/brands/tbx.jpg"
     data-settings='{"stellar-background-ratio": 0.6}' data-toggle="parallax-bg">
  <h2>
    WELCOME TO iZ BRANDS
  </h2>

  <p>
    iZ Brands is a powerful internet monitoring system to get instant access to online mentions and grow your sales.
  </p>
  <img alt="Bell - A perfect theme" class="gadgets-img hidden-md-down" src="${baseURL}/resources/img/brands/flow.png">
</div>
<!-- /Parallax -->


<!-- Call to Action -->
<section id="cta" class="cta">
  <div class="container">
    <div class="row">

      <div class="col-lg-12 col-sm-12 text-center">
        <h2>SIGN IN</h2>
        <p>Sign in to iZ Brands and discover what people are saying online about your brands, products, company or organization in real time.</p>
      </div>

      <div id="sign-grid" class="portfolio-grid">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-xs-12 text-center">
            <div class="card card-block">
              <a href="contact.html"><img alt="" src="${baseURL}/resources/img/brands/BrandCloud1.jpg">
                <div class="portfolio-over">
                  <div>
                    <h3 class="card-title">
                      Sign in to iZ Brands
                    </h3>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- /Call to Action -->