<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="req" value="${pageContext.request}" scope="request"/>
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" scope="request"/>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>iZ Brands - INTERNET MONITORING TOOLS FOR YOUR COMPANY</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">

  <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:image" content="">

  <!-- Favicon -->
  <link href="${baseURL}/resources/img/favicon.png"  type="image/png" rel="icon">
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/brands/lib/bootstrap/css/bootstrap.css">
  <link href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="${baseURL}/resources/js/production/brands/lib/owl/assets/owl.carousel.css">
  <link href="${baseURL}/resources/js/production/brands/css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="${baseURL}/resources/js/production/brands/lib/fancybox/jquery.fancybox.min.css">

  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>

</head>
<body id="wpb">
<tiles:insertAttribute name="header"/>
<tiles:insertAttribute name="content"/>
<tiles:insertAttribute name="footer"/>

<!-- Required JavaScript Libraries -->
<script src="${baseURL}/resources/js/production/brands/lib/jquery/jquery.min.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/jquery/jquery-migrate.min.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/superfish/hoverIntent.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/superfish/superfish.min.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/tether/js/tether.min.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/stellar/stellar.min.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/counterup/counterup.min.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/waypoints/waypoints.min.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/easing/easing.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/stickyjs/sticky.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/parallax/parallax.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/lockfixed/lockfixed.min.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/owl/owl.carousel.min.js"></script>
<script src="${baseURL}/resources/js/production/brands/lib/fancybox/jquery.fancybox.min.js"></script>

<!-- Template Specisifc Custom Javascript File -->
<script src="${baseURL}/resources/js/production/brands/js/custom.js"></script>
</body>
</html>