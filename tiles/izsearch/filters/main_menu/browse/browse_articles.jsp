<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<section class="art-section">
  <c:forEach var="item" items="${faves_list}" varStatus="item_status">
    <c:set var="title"
           value="${item.description==null?item.title:iz:trunc_words(item.description,\" \", 65)}"/>
    <c:set var="img_url" value="/images/faves/${item.id}_${item.title}.png"/>
    <c:set var="url" value="${item.directUrl==null?item.url:item.directUrl}"/>
    <c:set var="link" value="${iz:get_domain(url)}"/>
    <article class="art" style="clear:both; margin-bottom:20px;overflow: hidden">
      <div class="img">
        <a href="${url}" target="_blank">
          <img src="${img_url}" alt="image"/>
        </a>
      </div>
      <div class="data">
        <c:if test="${fn:length(title) > 50}" >
          <c:set var="title" value='${iz:trunc_words(title," ", 50)}&hellip;'/>
        </c:if>
        <h3 class="art-title"><a href="${url}">${title}</a></h3>
        <div class="art-sublink">
          <span class="pub-link">${link}</span>
        </div>
      </div>
    </article>
  </c:forEach>
</section>