<%--
  Created by IntelliJ IDEA.
  User: efanchik
  Date: 30.01.15
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/main-menu/browses.css"/>

<c:set var="categories" value="${requestScope.categories}" />
<c:set var="faves_list" value="${requestScope.favesList}" />
<c:set var="breadCrumb" value="${requestScope.breadCrumb}"/>
<c:set var="totalFaves" value="${requestScope.totalFaves}" />
<c:set var="pageHelper" value="${requestScope.pageHelper}"/>

<div id="category-wrapper" class="blogblock wpanel">
  <%@ include file="browse_breadcrumb.jsp"%>
  <div id="root-category">
    <%@ include file="browse_categories.jsp"%>
    <%@ include file="browse_articles.jsp"%>
    <c:if test="${not empty faves_list}">
    <%@ include file="browse_paginator.jsp"%>
    </c:if>
  </div>
</div>



