<%--
  Document   : forums
  Created on : Nov 11, 2014, 3:26:47 AM
  Author     : efanchik
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css" />
<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/shopping.css" />
<script type="text/javascript" src="${baseURL}/resources/js/production/lazyload/jquery.lazyload.min.js"></script>
<script type="text/javascript"
        src="${baseURL}/resources/js/production/lazyload/jquery.scrollstop.min.js"></script>

<c:set var="ads" value="${requestScope.ads}" scope="page"/>

<div id="wrapper">
  <div id="shopping-ads">
    <c:if test="${not empty ads}">
      <ul class="imagelist">
        <c:forEach var="item" items="${ads}" varStatus="status">
          <li class="item">
            <div id="item-wrapper">
              <div id="ad-img">
                <a href="${item.itemUrl}"><img class="lazy" src="${item.imageUrl}" alt="image" /></a>
              </div>
              <div id="ad-price">${item.currency}&nbsp;${item.price}</div>
              <div id="ad-rank">
                <c:set value="${iz:round(item.rating)}" var="rating" />
                <c:forEach var="limit" begin="0"  end="4" varStatus="status">
                  <c:choose>
                    <c:when test="${limit < rating}">
                      <i class="fa fa-star"></i>
                    </c:when>
                    <c:otherwise>
                      <i class="fa fa-star-o"></i>
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
              </div>
              <div id="ad-title">
                <a href="${item.itemUrl}">${item.title}</a>
              </div>
            </div>
          </li>
        </c:forEach>
      </ul>
    </c:if>
  </div>
  <script>
    $(document).ready(function () {
      $("img.lazy").lazyload({
        event: "scrollstop"
      });
    })
  </script>
</div>
