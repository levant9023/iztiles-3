<%-- 
    Document   : also_on
    Created on : Nov 7, 2014, 7:39:58 AM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.izsearch.model.WikiInfoLocal"%>
<%@ page import="java.util.ArrayList" %>

<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz"%>

<div id="wiki-info"></div>
<%--
<%
    WikiInfoLocal wiki = (WikiInfoLocal)request.getAttribute("wikiInfo");
    ArrayList<WikiInfoLocal> sections = new ArrayList<WikiInfoLocal>();
    ArrayList<String> titles = new ArrayList<String>();
    if(wiki!=null) {
        for (WikiInfoLocal section : wiki.getSections()) {
            sections.add(section);
            titles.add(section.getTitle());
        }
    }
    pageContext.setAttribute("sections", sections);
    pageContext.setAttribute("titles", titles);
%>


<c:set var="wikiInfo" value="${requestScope.wikiInfo}" />
<c:if test="${not empty wikiInfo}">
    <div id="wiki-info">
        <article class="art-wiki">
            <h3 class="underlinedh3">
                <c:set var='title' value='${iz:mark_words(wikiInfo.title, query)}' />
                <a href="${wikiInfo.url}">${title}</a>
            </h3>
            <c:set var="ds_l" value="${fn:length(wikiInfo.description)}" />
            <c:set var="descr" value="${iz:strip_str_pxl(wikiInfo.description,' ', 'Arial', 13, 500*4)}" />
            <c:set var="ds_sl" value="${fn:length(descr)}" />
            <c:set var="showMore" value='' />
            <c:if test="${ds_sl < ds_l}">
                <c:set var="showMore" value=' - <span class="sm">Show more</span>' />
            </c:if>
            <c:set var="showCompact" value=' - <span class="sm">Show compact</span>' />
            <c:set var='di_d' value='${iz:clear_descr(iz:text_normalize(wikiInfo.description))}' />
            <c:set var='di_d' value='${iz:mark_words(iz:html_quotes(di_d), query)}${showCompact}' />
            <p data-info='${di_d}'>
                <c:set var='descr' value='${iz:clear_descr(iz:text_normalize(descr))}' />
                ${iz:mark_words(iz:html_quotes(descr), query)}${showMore}
            </p>
        </article>
        <c:set var="sections" value="${sections}" />
        <c:set var='total' value='${iz:amount_words_to_fit(titles, 490, 0, 13, "Arial")}' />
        <c:if test='${total>0}'>
            <ul class="subtitle">
                <c:forEach var="section" items="${sections}" begin='0' end='${total-1}' varStatus='secStatus'>
                    <c:set var='delim' value=''/>
                    <c:if test='${secStatus.count < (total)}'>
                        <c:set var='delim' value='<span class="sep">|</span>'/>
                    </c:if>
                    <li>
                        <span><a href="${section.url}">${section.title}</a></span>
                        ${delim}
                    </li>
                </c:forEach>
            </ul>
        </c:if>
    </div>
</c:if>
--%>