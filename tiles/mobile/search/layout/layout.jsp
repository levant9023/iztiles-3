<%@ page import="org.omg.CORBA.Request" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE HTML>
<c:set var="query" value="${empty requestScope.q ? '' : requestScope.q}" scope="request"/>
<c:set var="lang" value="${empty requestScope.locale ? 'en' : fn:substring(requestScope.locale,0, 2)}" scope="request"/>
<c:set var="req" value="${pageContext.request}" scope="request"/>
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" scope="request"/>
<c:set var="meta_title" value="${requestScope.meta_title}" scope="request"/>
<html lang="<c:out value="${lang}"/>">
	<head>
		<tiles:insertAttribute name="head"/>
	</head>
	<body>
		<div id="wrapper" class="container-fluid">
			<tiles:insertAttribute name="header"/>
			<div id="rsads-wrapper" class="hide">
				<div class="popover-content">
					<div id="redmine-report">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="bugfix-title">Report bugs</h3>
							</div>
							<div class="panel-body">
								<form id="form-redmine" class="izwidget" role="form">
									<div class="form-group">
										<label for="inputUserName" class="control-label">Your name (required)*:</label>
										<input type="text" class="form-control" id="inputUserName" name="username" value="">
										<span id="username_message" class="message"></span>
									</div>
									<div class="form-group">
										<label for="inputBugEmail" class="control-label">Your email (required)*:</label>
										<input type="email" class="form-control" id="inputBugEmail" name="usermail">
										<span id="usermail_message" class="message"></span>
									</div>
									<div class="form-group">
										<label for="inputBugSubject" class="control-label">Subject (required) *:</label>
										<input type="text" class="form-control" id="inputBugSubject" name="bugsubject">
										<span id="bugsubject_message" class="message"></span>
									</div>
									<div class="form-group">
										<label for="inputBugDescr" class="control-label">Bug description (required)*:</label>
										<textarea class="form-control" rows="2" id="inputBugDescr" name="bugdescr"></textarea>
										<span id="bugdescr_message" class="message"></span>
									</div>
									<div class="form-group">
										<div class="btn-send">
											<span id="redmine-result-message" class="message"></span>
											<button id="btn-redmine" type="button" class="btn btn-primary btn-block">Send</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<section id="middle">
				<tiles:insertAttribute name="content"/>
			</section>
			<footer id="footer">
				<ul class="nav">
					<li><a href="${baseURL}/about.html">About</a></li>
					<li><a href="http://blog.izsearch.com">Blog</a></li>
					<li><a id="bbtn-api" href="${baseURL}/api.html">News API</a></li>
					<li><a id="bbtn-advertise" href="${baseURL}/advertise.html">iZ Ads</a></li>
					<li><a id="bbtn-izebra" href="${baseURL}/app.html">iZebra App</a></li>
					<!--<li><a id="bbtn-engine" data-toggle="modal" data-target="#btn_engine"><i class="btn-suscr glyphicon glyphicon-plus"></i>Add to Search</a></li>-->
					<li><a id="bbtn-homepage" href="/">Set as Home</a></li>
				</ul>
			</footer>
		</div>
		<script>
			(function (i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o), m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
			ga('create', 'UA-58665066-1', 'auto');
			ga('require', 'linkid', 'linkid.js');
			ga('send', 'pageview');
			setTimeout("ga('send', 'event', 'Pageview', 'Page visit 15 sec. or more')", 1500);
		</script>
	</body>
</html>
