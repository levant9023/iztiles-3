<%-- 
    Document   : header
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<c:set var="meta_title" value="${requestScope.meta_title}" />

<head>
  <base href="${baseURL}/">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>iZSearch search engine | <c:out value="${meta_title} Mobile version." default="iZSearch search engine. Mobile version."/> </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="izSearch search engine, the new private way to search the web. Search engine that finds and returns relevant web sites, images, videos and realtime results.">
  <meta name="keywords" content="izsearch, izsearch home, izsearch home page, izsearch search, izsearch web, news, blog, forum, health, sports, travel, tech, entertainment, video, image">
  <link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/mobile/subpages/subpages.css"/>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquerypp.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/mvc/classes.js"></script>
  <script type="text/javascript"
          data-main="${baseURL}/resources/js/production/mobile/js/main_app.js"
          src="${baseURL}/resources/js/production/mobile/js/require.js">
  </script>
</head>