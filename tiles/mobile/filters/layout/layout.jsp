<%@ page import="org.omg.CORBA.Request" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE HTML>
<c:set var="query" value="${empty requestScope.q ? '' : requestScope.q}" scope="request"/>
<c:set var="lang" value="${empty requestScope.locale ? 'en' : fn:substring(requestScope.locale,0, 2)}" scope="request"/>
<c:set var="req" value="${pageContext.request}" scope="request"/>
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" scope="request"/>
<c:set var="normURL" value="${req.scheme}://${req.serverName}${req.contextPath}" scope="request"/>
<c:set var="meta_title" value="${requestScope.meta_title}" scope="request"/>

<html lang="<c:out value="${lang}"/>">
	<head>
		<tiles:insertAttribute name="head"/>
	</head>
<body>
	<!-- wrapper -->
	<div id="wrapper" class="container-fluid">
		<tiles:insertAttribute name="header"/>
		<div id="rsads-wrapper" class="hide">
			<div id="rmn" class="popover-content">
				<div id="redmine-report">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="bugfix-title">Report bugs</h3>
						</div>
						<div class="panel-body">
							<form id="form-redmine" class="izwidget" role="form">
								<div class="form-group">
									<label for="inputUserName" class="control-label">Your name (required)*:</label>
									<input type="text" class="form-control" id="inputUserName" name="username" value="">
									<span id="username_message" class="message"></span>
								</div>
								<div class="form-group">
									<label for="inputBugEmail" class="control-label">Your email (required)*:</label>
									<input type="email" class="form-control" id="inputBugEmail" name="usermail">
										<span id="usermail_message" class="message"></span>
								</div>
								<div class="form-group">
									<label for="inputBugSubject" class="control-label">Subject (required) *:</label>
									<input type="text" class="form-control" id="inputBugSubject" name="bugsubject">
									<span id="bugsubject_message" class="message"></span>
								</div>
								<div class="form-group">
									<label for="inputBugDescr" class="control-label">Bug description (required)*:</label>
									<textarea class="form-control" rows="2" id="inputBugDescr" name="bugdescr"></textarea>
									<span id="bugdescr_message" class="message"></span>
								</div>
								<div class="form-group">
									<div class="btn-send">
										<span id="redmine-result-message" class="message"></span>
										<button id="btn-redmine" type="button" class="btn btn-primary btn-block">Send</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="slide-wrapper">
			<div id="carousel-block">
				<div id="hcancarousel"></div>
			</div>
		</div>
		<section id="middle">
			<tiles:insertAttribute name="content"/>
		</section>
		<div class="container preloader-container">
			<div class="preloader-item">
				<div class="loader03"></div>
			</div>
		</div>
		<%-- <tiles:insertAttribute name="footer"/> --%>
	</div>
	<!-- wrapper -->
	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
		ga('create', 'UA-58665066-1', 'auto');
		ga('require', 'linkid', 'linkid.js');
		ga('send', 'pageview');
		setTimeout("ga('send', 'event', 'Pageview', 'Page visit 15 sec. or more')", 1500);
	</script>

	<h1 class="grob">${meta_title}</h1>
	<h2 class="grob">${meta_title}</h2>


	<div id="rss_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rss_modal ">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal_share trigger-share">
						<ul>
							<li><a class="fb" href="" target="_blank"><i class="fa fa-facebook" ></i></a></li>
							<li><a class="tw" href="" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="google" href="" target="_blank"><i class="fa fa-google-plus"></i></a></li>
							<li><a class="pin" href="" target="_blank"><i class="fa fa-pinterest"></i></a></li>
						</ul>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="top-content"><div class="amazon-slider"></div></div>
					<div id="carousel-modal" class="carousel slide" data-wrap="true" data-interval="false" data-ride="carousel">
						<div class="carousel-inner" role="listbox"></div>

						<a class="left carousel-control" href="#carousel-modal" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-modal" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
