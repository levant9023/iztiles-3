<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="combined" value="${requestScope['snippets']['combined']}"/>
<c:set var="list1" value="${requestScope['snippets']['list1']}"/>
<c:set var="list2" value="${requestScope['snippets']['list2']}"/>
<c:set var="query" value="${requestScope['q']}"/>

<c:set var="nbf" value="false"/>
<c:choose>
	<c:when test="${(not empty requestScope.wpath) and (requestScope.wpath eq '/news.html')}">
		<c:set var="nbf" value="true"/>
	</c:when>
	<c:when test="${(not empty requestScope.wpath) and (requestScope.wpath eq '/blog.html')}">
		<c:set var="nbf" value="true"/>
	</c:when>
	<c:when test="${(not empty requestScope.wpath) and (requestScope.wpath eq '/forum.html')}">
		<c:set var="nbf" value="true"/>
	</c:when>
	<c:otherwise>
		<c:set var="nbf" value="false"/>
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${nbf eq false}">
		<c:if test="${not empty list1}">
			<section class="sec">
				<c:forEach var="article" items="${list1}" varStatus="sec_status">
					<c:set var="title" value="${iz:text_normalize(article.title)}"/>
					<c:set var="descr" value="${iz:text_normalize(article.description)}"/>
					<c:if test="${not empty query}">
						<c:set var="descr" value="${iz:mark_words(descr, query)}"/>
						<c:set var="title" value="${iz:mark_words(title, query)}"/>
					</c:if>
					<c:set var="url" value="${article.url}"/>
					<c:set var="link" value='${iz:top_private_domain(article.url)}'/>
					<c:set var="img" value="${article.imageUrl}"/>
					<c:set var="date" value="${article.date}"/>
					<article class="art">
						<c:if test="${iz:is_img(img) == true}">
							<div class="img">
								<img src="<c:out value="${img}" default="" escapeXml="false"/>"/>
							</div>
						</c:if>
						<div class="data">
							<h3 class="art-title"><a href="${url}">${title}</a></h3>

							<div class="art-sublink">
								<span class="pub-date"><fmt:formatDate type="date" value="${date}"/></span>
								<span class="delim">|</span>
								<span class="pub-link">${link}</span>
							</div>
							<c:if test="${fn:length(descr) > 160}">
								<c:set var="descr_cuted" value='${iz:trunc_words(iz:text_normalize(article.description)," ", 160)}&hellip;'/>
							</c:if>
							<div class="art-descr">${descr}</div>
							<div class="art-footer"></div>
						</div>
					</article>
				</c:forEach>
			</section>
		</c:if>
		<c:if test="${combined eq true}">
			<div class="delimiter"></div>
			<c:if test="${not empty list2}">
				<section class="sec">
					<c:forEach var="article" items="${list2}" varStatus="sec_status">
						<c:set var="title" value="${iz:text_normalize(article.title)}"/>
						<c:set var="descr" value="${iz:text_normalize(article.description)}"/>
						<c:if test="${not empty query}">
							<c:set var="descr" value="${iz:mark_words(descr, query)}"/>
							<c:set var="title" value="${iz:mark_words(title, query)}"/>
						</c:if>
						<c:set var="url" value="${article.url}"/>
						<c:set var="link" value='${iz:top_private_domain(article.url)}'/>
						<c:set var="img" value="${article.imageUrl}"/>
						<c:set var="date" value="${article.date}"/>
						<article class="art">
							<c:if test="${iz:is_img(img) == true}">
								<div class="img">
									<img src="<c:out value="${img}" default="" escapeXml="false"/>"/>
								</div>
							</c:if>
							<div class="data">
								<h3 class="art-title"><a href="${url}">${title}</a></h3>
								<div class="art-sublink">
									<span class="pub-date"><fmt:formatDate type="date" value="${date}"/></span>
									<span class="delim">|</span>
									<span class="pub-link">${link}</span>
								</div>
								<c:if test="${fn:length(descr) > 160}">
									<c:set var="descr_cuted" value='${iz:trunc_words(iz:text_normalize(article.description)," ", 160)}&hellip;'/>
								</c:if>
								<div class="art-descr">${descr}</div>
								<div class="art-footer"></div>
							</div>
						</article>
					</c:forEach>
				</section>
			</c:if>
		</c:if>
	</c:when>
</c:choose>
