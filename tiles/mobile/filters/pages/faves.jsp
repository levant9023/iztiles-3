<%--
    Document   : browses
    Created on : 08.01.2015, 17:54:07
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz"%>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/faves.css" />

<c:set var="faves_map" value="${requestScope.faves}" scope="page"/>
<c:if test="${not empty faves_map}" >
  <c:set var="faves" value="${faves_map['faves']}" />
  <div id="buttons-filter" class="blogblock wpanel">
      <%-- ${faves_map['debstr']} --%>
    <c:if test="${not empty faves}" >
      <c:forEach var="tab" items="${faves}">
        <c:forEach var="fave" items="${tab.value}" varStatus="faves_status">
          <c:set var="cap" value="${fn:toUpperCase(fn:substring(fave.key, 0, 1))}" />
          <c:set var="body" value="${fn:substring(fave.key, 1, fn:length(fave.key))}" />
          <ul class="faves-list">
            <h3 class="underlinedh3"><c:out value="${cap}${body}" default="N/A"/></h3>
            <c:set var="faveslist2draw" value="${fave.value}" scope="request"/>
            <jsp:include page="browses.jsp"/>
          </ul>
        </c:forEach>

      </c:forEach>
    </c:if>
  </div>
</c:if>
