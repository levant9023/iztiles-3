<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
  <title>iZSearch Keywords Cart</title>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/css/bootstrap.min.css"/>
  <script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/js/bootstrap.js"></script>
  <link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/login.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
<div class="header">
  <a class="logo" href="/"><img src="/resources/img/logo.svg" alt="logo"><br>© iZSearch</a>
  <h1>Picking the Keywords</h1>
  <a id="logout" href="/j_spring_security_logout" class="btn btn-primary"><span class="glyphicon glyphicon-log-out"></span> Log out</a>
</div>
<div class="container-fluid">
  <h1>KEYWORDS SEARCH</h1>
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label class="control-label " for="keyword">KEYWORDS SEARCH</label>
        <input class="form-control" id="keyword" name="keyword" type="text" onkeydown = "if (event.keyCode == 13)document.getElementById('searchKeywordsButton').click()"/>
      </div>
      <div class="form-group">
        <div>
          <button id="searchKeywordsButton" class="btn btn-primary btn-lg btn-block" name="submit">Search</button>
        </div>
      </div>
    </div>
    <div class="col-md-9">
      <form method="post">
        <div class="form-group " id="checkBoxes"></div>
        <div class="form-group">
          <button class="btn btn-primary " name="submit" type="submit" id="buyWords">Add</button>
        </div>
      </form>
    </div>
  </div>
</div>
</body>
</html>
